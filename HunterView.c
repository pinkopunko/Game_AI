// HunterView.c ... HunterView ADT implementation
// COMP1927 15s2 ... basic HunterView (supplied code)
// Code by TheGroup from COMP1927 14s2

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "Globals.h"
#include "Game.h"
#include "GameView.h"
#include "HunterView.h"
#include "Places.h"
#include "Map.h"

// Representation of the Hunter's view of the game

struct hunterView {
    GameView game;
};


// BFS Search - function to test Adjacent cities. Used in hunter.c (decideHunterMove) function.
int areAdjacent (HunterView hunterView,LocationID v1,LocationID v2){
    GameView game = hunterView->game;
    Map map = getMap (game);
    return isConnected (map,v1,v2);
}

// What are the specified player's next possible moves
LocationID *validAdjacent(HunterView currentView, int *numLocations,
                           LocationID from, int road, int rail, int sea)
{
    int i, numValidLocations, index;
    LocationID forbidden;
    LocationID *validLocations;

    LocationID *locations = connectedLocations(currentView->game,
                                               numLocations,
                                               from,
                                               whoAmI(currentView),
                                               getRound(currentView->game),
                                               road, rail, sea);
    
    forbidden = ST_JOSEPH_AND_ST_MARYS;

    numValidLocations = 0;
    for(i = 0; i < (*numLocations); i++){
        if(locations[i] != forbidden){
            numValidLocations++;
        }
    }

    index = 0;
    validLocations = malloc(sizeof(LocationID) * numValidLocations);
    for(i = 0; i < *numLocations; i++){
        if(locations[i] != forbidden){
            validLocations[index] = locations[i];
            index++;
        }
    }

//    free(locations);
    *numLocations = index;

    return validLocations;
}


// Creates a new HunterView to summarise the current state of the game
HunterView newHunterView(char *pastPlays, PlayerMessage messages[])
{
    //REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    HunterView hunterView = malloc(sizeof(struct hunterView));
    hunterView->game = newGameView(pastPlays, messages);
    return hunterView;
}


// Frees all memory previously allocated for the HunterView toBeDeleted
void disposeHunterView(HunterView toBeDeleted)
{
    //COMPLETE THIS IMPLEMENTATION
    free(toBeDeleted->game);
    free( toBeDeleted );
}


//// Functions to return simple information about the current state of the game

// Get the current round
Round giveMeTheRound(HunterView currentView)
{
    return getRound(currentView->game);
}

// Get the id of current player
PlayerID whoAmI(HunterView currentView)
{
    return getCurrentPlayer(currentView->game);
}

// Get the current score
int giveMeTheScore(HunterView currentView)
{
    return getScore(currentView->game);
}

// Get the current health points for a given player
int howHealthyIs(HunterView currentView, PlayerID player)
{
    return getHealth(currentView->game, player);
}

// Get the current location id of a given player
LocationID whereIs(HunterView currentView, PlayerID player)
{
    return getLocation(currentView->game, player);
}

//// Functions that return information about the history of the game

// Fills the trail array with the location ids of the last 6 turns
void giveMeTheTrail(HunterView currentView, PlayerID player,
                            LocationID trail[TRAIL_SIZE])
{
    getHistory(currentView->game, player, trail);
}

//// Functions that query the map to find information about connectivity

// What are my possible next moves (locations)
LocationID *whereCanIgo(HunterView currentView, int *numLocations, int road, int rail, int sea)
{
    return whereCanTheyGo(currentView,
                               numLocations,
                               getCurrentPlayer(currentView->game),
                               road, rail, sea);
}

// What are the specified player's next possible moves
LocationID *whereCanTheyGo(HunterView currentView, int *numLocations,
                           PlayerID player, int road, int rail, int sea)
{
    int i, numValidLocations, index;
    LocationID forbidden;
    LocationID *validLocations;

    LocationID *locations = connectedLocations(currentView->game,
                               numLocations,
                               getLocation(currentView->game, player),
                               player,
                               getRound(currentView->game),
                               road, rail, sea);

    forbidden = ST_JOSEPH_AND_ST_MARYS;

    numValidLocations = 0;
    for(i = 0; i < (*numLocations); i++){
        if(locations[i] != forbidden){
            numValidLocations++;
        }
    }

    index = 0;
    validLocations = malloc(sizeof(LocationID) * numValidLocations);
    for(i = 0; i < *numLocations; i++){
        if(locations[i] != forbidden){
            validLocations[index] = locations[i];
            index++;
        }
    }

    free(locations);
    *numLocations = index;

    return validLocations;
}

int dracback(HunterView currentView)
{
    return doubleback(currentView->game);
}

LocationID *whereisdracula(HunterView current, int *number)
{
    return getDracula(current->game, number);
}
