// hunter.c
// Implementation of your "Fury of Dracula" hunter AI
 
#include <stdlib.h>
#include <stdio.h>
#include "Game.h"
#include "GameView.h"
#include "HunterView.h"
#include "assert.h"
#include "Map.h"
 
/* FUNCTIONS OF THE HUNTER
 *
 * 1. Path towards last sign of dracula
 *    - need "distance chart" for every possible move (providing score)
 *       + remember trains!
 *
 * 2. favor alternative path over same path as other hunter
 *    - just score/2
 *
 * 3. Sit to heal vs let self die and be ported to hospital (on <5/<3 hp)
 *    - if within certain distance of hospital on death?
 *
 * 4. All sit for research if no clues in a long while (x amount of turns)
 *    - more favourable if 3/4 are missing hp
 *    - only after round 6 (else no clue is found)
 *
 * 5. If sea then sit on ports around
 *    - scatter to cover most space
 */
 
/*  Thoughts for using possible drac moves to track down dracula
 *  1. stagger around in hope of discovering drac until move 6
 *  2. Research to reduce possible drac positions
 *  3. move towards closest possible drac position
 *      - calculate other hunters paths too so no one choses the same path/pos & does the work twice?
 *  4. attempt to cut as many of the possible drac positions as possible
 *      - "trap" the last known position in a circle to find more trail
 *  5. 
 */

LocationID mostLikelySea (HunterView gameState, LocationID from, int howFarFromLastSeen);
int findPath(HunterView gameState, LocationID src, LocationID dest);
int pathLength(HunterView gameState, LocationID src, LocationID dest);
 
//main function to decide move
void decideHunterMove(HunterView gameState)
{
    
    
    // -----------------------------------------------------------------------------
    //
    // Initialise variables + Print information about board
    //
    // -----------------------------------------------------------------------------
    
    // Who Am I?
    PlayerID currentHunter = whoAmI(gameState);
    
    // Where Am I?
    LocationID iAmHere = whereIs(gameState, currentHunter);
    
    int numPossibleMoves = 0;
    int posNum;
    
    // this is my trail
    LocationID trail[TRAIL_SIZE];
    giveMeTheTrail(gameState, currentHunter, trail);
     
    // gets drac's trail to see if it's still fresh.
    LocationID dracTrail[TRAIL_SIZE];
    giveMeTheTrail(gameState, PLAYER_DRACULA, dracTrail);
    
    // fresh:
    // 0: no clue where he is
    // 1: we've got a trail
    int counter,fresh = 0;
    
    int freshLoc = -1;
    
    printf("\nDraculas trail:");
    
    //iterate through drac's trail
    for (counter = 0; counter < TRAIL_SIZE; counter++) {
        // if we come across a non-hidden location on sea/land
        if (dracTrail[counter] >= 0 && dracTrail[counter] <= MAX_MAP_LOCATION){
            //change the fresh counter
            fresh = 1;
            
            if (freshLoc == -1) {
                freshLoc = counter;
            }
            
        }
        // print out each of his trail positions
        printf(" %s",idToName(dracTrail[counter]));
    }
    printf("\n");
    
 
   // IF not first turn OR IF we have a fresh trail of dracula
    if(trail[0]>-1 || fresh == 1){
        // a list of locations where our hunter can go.
        LocationID *possibleMoves = whereCanIgo(gameState, &numPossibleMoves, 1, 1, 1);
        LocationID move;
        int moveScore, topScore = 0;
        int i, j, k, c;
 
        //prints info
        LocationID *drachistory = whereisdracula(gameState, &posNum);
        printf("Dracula can be at %d possible locations:\n", posNum);

//        if (posNum <= 30)
//            for (i=0;i<posNum;i++)
//                printf("%s ", idToName(drachistory[i]));

//        printf("\n---------------------------------------\n");

        printf("Round: %d\t\tScore: %d\t\tHealth: %d\t\tLoc: %s\n",
        giveMeTheRound(gameState), giveMeTheScore(gameState), 
        howHealthyIs(gameState, currentHunter), idToName(whereIs(gameState, currentHunter)));
         
        printf("HunterLoc:\t\tG: %s\t\tS: %s\t\tH: %s\t\tM: %s\n",
        idToName(whereIs(gameState, 0)), idToName(whereIs(gameState, 1)),
        idToName(whereIs(gameState, 2)), idToName(whereIs(gameState, 3)));
         
        printf("HunterHP:\t\tG: %d\t\tS: %d\t\tH: %d\t\tM: %d\n",
        howHealthyIs(gameState, 0), howHealthyIs(gameState, 1),
        howHealthyIs(gameState, 2), howHealthyIs(gameState, 3));
         
        printf("Trail: %s %s %s %s %s %s\n", idToName(trail[0]), idToName(trail[1]), 
            idToName(trail[2]), idToName(trail[3]), idToName(trail[4]), idToName(trail[5]));
         
        printf("Possible Move:");
        for(i=0;i<numPossibleMoves;i++){
            printf(" %s", idToName(possibleMoves[i]));
        }
        printf(" - Amount: %d", numPossibleMoves);
        printf("\nDraculas location: %s\n", idToName(whereIs(gameState, PLAYER_DRACULA)));
        printf("Registering move - ");
        
        // -----------------------------------------------------------------------------
        //
        // Checks for death
        //
        // -----------------------------------------------------------------------------
        
        // checks IF we've died via:
        // IF we've exceeded the first turn && IF our current location is at the Hospital
        if (trail[2] > -1 && whereIs(gameState, currentHunter) == ST_JOSEPH_AND_ST_MARYS){
            
            // a list of locations we could have reached from our previous location
            LocationID *possibleFromPrev = validAdjacent(gameState, &numPossibleMoves, trail[1], 1, 1, 1);
            
            // died:
            // 0: we're still alive from last turn
            // 1: we died
            int died = 1;
            
            // traverse through all the possible locations from my previous location
            int count = 0;
            for (count = 0; count < numPossibleMoves; count++) {
                
                // IF our move from the previous spot to the hospital was valid, we're not considered dead yet
                if (iAmHere == possibleFromPrev[count]) {
                    died = 0;
                    break;
                }
            }
            
            // IF our health is full and
            if (howHealthyIs(gameState, currentHunter) == GAME_START_HUNTER_LIFE_POINTS && trail[1] != ST_JOSEPH_AND_ST_MARYS) {
                died = 1;
            } else {
                died = 0;
            }
             
            if (died) {
                printf("I died at %s and am now at %s\n",idToName(trail[1]), idToName(iAmHere));
                registerBestPlay(idToAbbrev(SZEGED),"I'mma find you, And i'mma giv u sum Mcluvin for killin me.");
                return;
            }
             
        }
 
        // -----------------------------------------------------------------------------
        //
        // WHEN dracula's location is known.
        //
        // -----------------------------------------------------------------------------
        
        // IF we know where Dracula's exact location is THIS TURN
        if ( whereIs(gameState, PLAYER_DRACULA) <= MAX_MAP_LOCATION && whereIs(gameState, PLAYER_DRACULA) >= 0){
             
            printf("We know...\n");
             
            if ( whereIs(gameState, whoAmI(gameState)) == whereIs(gameState, PLAYER_DRACULA)) {
                registerBestPlay(idToAbbrev(whereIs(gameState, PLAYER_DRACULA)), "I can FEEEEL YOU MUTH LUVA");
                return;
            }
             
            move = findPath(gameState, whereIs(gameState,whoAmI(gameState)), whereIs(gameState, PLAYER_DRACULA));
            printf("Going to %s\n",idToName(move));
            
            registerBestPlay(idToAbbrev(move),"I can seeeee you.");
             
            free(possibleMoves);
            
            
        // -----------------------------------------------------------------------------
        //
        // WHEN we've got a Fresh Trail, but don't know Drac's exact location this turn
        //
        // -----------------------------------------------------------------------------
             
        // IF a part of draculas trail is known
        } else if ( fresh == 1 ){
            
            LocationID dracPossible[TRAIL_SIZE];

            // if your a certain hunter, only consider which sea the previous hunters are going to.
            LocationID newDest;
            int traversal = 0;
            int found = 0;
            
            // variable for if he went to sea
            int dracWentToSea = 0;
            
            // how far from his last exact known location
            int howFarFromLastSeen = 0;
            
            // how far from the sea is he
            int fromRecentSea = 0;
            
            // how many turns were spent at sea?
            int timeAtSea = 0;
            
            // how many seas did he go to?
            int numSeas = 0;
            
            // prev sea
            LocationID prevSea = -1;
            int disFromPreSea = 1;
            
            
            printf("Oh is that your trail?\n");
            
            // traverse through drac's trail
            for (counter = freshLoc; counter > -1; counter--) {
                
                if (counter == freshLoc) {
                    dracPossible[counter] = dracTrail[freshLoc];
                    continue;
                }
                
                // IF drac has used a hide and it was not his oldest move
                if (dracTrail[counter] == HIDE && counter < freshLoc) {
                
                    // IF drac was a sea before hiding
                    if (dracTrail[counter + 1] == SEA_UNKNOWN ){
                        // increment the num turns drac spent at sea
                        timeAtSea++;
                    }
                    
                    // since drac hid, we decrement how far he could have gone from his last seen
                    howFarFromLastSeen--;
                    
                // ELSE IF drac went to sea and he's not at a sea right now
                } else if (dracWentToSea && dracTrail[counter] != SEA_UNKNOWN){
                    // increment how far drac could be from the recent sea
                    fromRecentSea++;
                    timeAtSea++;
                    disFromPreSea++;
                    
                // ELSE IF drac went from one sea to another sea
                } else if (dracWentToSea && dracTrail[counter] == SEA_UNKNOWN){
                    
                    dracPossible[counter] = mostLikelySea(gameState, prevSea, disFromPreSea);
                    prevSea = dracPossible[counter];
                    
                    numSeas ++;
                    timeAtSea++;
                    
                    disFromPreSea = 1;
                }
                
                
                // IF drac trail includes an unknown sea
                if (dracTrail[counter] == SEA_UNKNOWN && dracWentToSea != 1){
                    // drac went to sea
                    dracWentToSea = 1;
                    
                    dracPossible[counter] = mostLikelySea(gameState, dracTrail[counter + 1], howFarFromLastSeen + 1);
                    prevSea = dracPossible[counter];
                    
                    
                    numSeas++;
                    
                    // increment the num turns drac spent at sea
                    timeAtSea++;
                }

                howFarFromLastSeen++;
                
            }
            
            printf("Last saw Dracula at %s\n",idToName(dracTrail[freshLoc]));
             
            printf("how far from last seen = %d\n",howFarFromLastSeen);
            printf("seas travelled = %d\n",timeAtSea);
            
            int timeAtLand = howFarFromLastSeen - fromRecentSea;
            
            if (dracWentToSea == 1) {
                printf("He went to the sea captain\n");
                
                LocationID destination = -1;
                if (dracTrail[0] == SEA_UNKNOWN) {
                    
                    counter = 0;
                    while (dracPossible[counter] > MAX_MAP_LOCATION || dracPossible[counter] < MIN_MAP_LOCATION) {
                        counter++;
                    }
                    
                    destination =  dracPossible[counter];
                    
                    if (destination != -1) {
                        printf("--- SEA = %s ---\n",idToName(destination));
                        
                        move = findPath(gameState, whereIs(gameState,whoAmI(gameState)),destination);
                        registerBestPlay(idToAbbrev(move),"SWIMMING ARE YOU?");
                        free(possibleMoves);
                        return;
                    } else {
                        printf("fail1\n");
                    }
                    
                    
                } else {
                    
                    
                    counter = 0;
                    while (dracPossible[counter] > MAX_MAP_LOCATION || dracPossible[counter] < MIN_MAP_LOCATION) {
                        counter++;
                    }
                    
                    destination =  dracPossible[counter];
                    
//                    howFarFromLastSeen = fromRecentSea;
//                    
//                    destination =  mostLikelySea(gameState, dracTrail[counter], howFarFromLastSeen);
                    
                    
                    if (destination != -1) {
                        printf("--- SEA = %s ---\n",idToName(destination));

                        for (traversal = 0; traversal < NUM_MAP_LOCATIONS; traversal++) {
                            
                            if (pathLength(gameState, destination, traversal) == timeAtLand && traversal != dracTrail[counter]) {
                                move = findPath(gameState, whereIs(gameState,whoAmI(gameState)),traversal);
                                registerBestPlay(idToAbbrev(move),"was the swim ok?");
                                free(possibleMoves);
                                return;
                            }
                        }
                        
                    } else {
                        printf("fail2\n");
                    }
                    
                }
                
            }
            
            printf("He didn't end up at the sea captain\n");
            
            
            // set a loc to drac's last sighting
            
            if (found) {
                newDest = traversal;
            } else {
                newDest = dracTrail[counter];
            }
            
            
            move = findPath(gameState, whereIs(gameState,whoAmI(gameState)),newDest);
            printf("Going to %s", idToName(move));
            registerBestPlay(idToAbbrev(move),"I can smell you.");
             
            free(possibleMoves);
 
            
            
            
            
            
        } else if (dracTrail[0] == SEA_UNKNOWN || dracTrail[1] == SEA_UNKNOWN || dracTrail[2] == SEA_UNKNOWN || dracTrail[3] == SEA_UNKNOWN || dracTrail[4] == SEA_UNKNOWN){
        
            printf("sea unknown? I like a good challenge\n");
            LocationID destination = -1;
            int length = 0;
            int counter = 0;
            while (dracTrail[counter] != SEA_UNKNOWN) {
                counter++;
            }
            printf("cter = %d\n",counter);
            
            while (destination == -1 && length < NUM_MAP_LOCATIONS) {
                
                destination =  mostLikelySea(gameState, whereIs(gameState,whoAmI(gameState)), length);
                
                length++;
            }
            
            printf("%s\n",idToName(destination));
            
            if (counter == 0) {
                move = findPath(gameState, whereIs(gameState,whoAmI(gameState)),destination);
                printf("Going to %s", idToName(move));
                registerBestPlay(idToAbbrev(move),"splish splosh ahtum rekcof.");
                
                free(possibleMoves);
                return;
            }
            
            int counter2 = 0;
            while (pathLength(gameState, destination, counter2) != counter && counter2 < NUM_MAP_LOCATIONS) {
                counter2++;
            }
            
            
            move = findPath(gameState, whereIs(gameState,whoAmI(gameState)),counter2);
            printf("Going to %s", idToName(move));
            registerBestPlay(idToAbbrev(move),"I can sense you.");
            
            free(possibleMoves);
            return;
            
            
        // else if we dunno drac's location
        } else {
            printf("Let's look around\n  ");
            //Chose move
            for(i=0;i<numPossibleMoves;i++){
 
                //legal move backup
                if(possibleMoves[i] > 70 && possibleMoves[i] < 0){
                    numPossibleMoves--;
                    continue;
                }
 
                //checks score of move (random & worth less if in trail/other hunter is there)
                moveScore = rand()%100000 + 1000000;
                moveScore = possibleMoves[i] == trail[1] ? moveScore*0.1 : moveScore;
                moveScore = possibleMoves[i] == trail[2] ? moveScore*0.3 : moveScore;
                moveScore = possibleMoves[i] == trail[3] ? moveScore*0.6 : moveScore;
                moveScore = possibleMoves[i] == trail[4] ? moveScore*0.8 : moveScore;
                moveScore = possibleMoves[i] == trail[5] ? moveScore*0.95 : moveScore;
                moveScore = possibleMoves[i] == whereIs(gameState, 0) ? moveScore*0.01 : moveScore;
                moveScore = possibleMoves[i] == whereIs(gameState, 1) ? moveScore*0.01 : moveScore;
                moveScore = possibleMoves[i] == whereIs(gameState, 2) ? moveScore*0.01 : moveScore;
                moveScore = possibleMoves[i] == whereIs(gameState, 3) ? moveScore*0.01 : moveScore;
                moveScore = ((idToType(possibleMoves[i]) == 2)?moveScore/2:moveScore);
                 
                //aims to take away possible positions asap!
//                LocationID *drachistory = whereisdracula(gameState, &posNum);
                for (j=0;j<posNum;j++){
                    if (possibleMoves[i] == drachistory[j]){
                        moveScore *= 5;
                        break;
                    }
                }

                //worth more if port
                int numOptions;
                LocationID *options = validAdjacent(gameState, &numOptions, possibleMoves[i], 1, 1, 1);
 
                for (j=0;j<numOptions;j++){
                    if (idToType(options[j]) == 2) moveScore *= 1.1;

                    for (k=0;k<posNum;k++){
                        if (options[j] == drachistory[k]){
                            moveScore *= 1.2;
                        }
                    }
 
                    for (k=0; k<4; k++){
                        int numOptions2;
                        LocationID *options2 = validAdjacent(gameState, &numOptions2, options[j], 1, 1, 1);
                        LocationID trailhunter[6];
                        giveMeTheTrail(gameState, k, trailhunter);

                        for (c=0;c<posNum;c++){
                            if (options2[k] == drachistory[c]){
                                moveScore *= 1.1;
                            }
                        }

                        for (c=0; c<6;c++){
                            if (options[j] == trailhunter[c]) moveScore *=0.9;
                        }
    
                        for (c=0; c<numOptions2; c++){
                            if (currentHunter != k && options2[c] == whereIs(gameState, k)) moveScore *= 0.95;
                        }

                        if (currentHunter != k && options[j] == whereIs(gameState, k)) moveScore *= 0.9;
                    }
                }
 
                if (currentHunter == 0){
                    if (possibleMoves[i] == PRAGUE ||
                        possibleMoves[i] == NUREMBURG ||
                        possibleMoves[i] == MUNICH ||
                        possibleMoves[i] == VENICE ||
                        possibleMoves[i] == ZAGREB ||
                        possibleMoves[i] == VIENNA ||
                        possibleMoves[i] == BUDAPEST ||
                        possibleMoves[i] == SZEGED ||
                        possibleMoves[i] == SARAJEVO ||
                        possibleMoves[i] == BELGRADE ||
                        possibleMoves[i] == VALONA ||
                        possibleMoves[i] == SALONICA ||
                        possibleMoves[i] == SOFIA ||
                        possibleMoves[i] == VARNA ||
                        possibleMoves[i] == CONSTANTA ||
                        possibleMoves[i] == GALATZ ||
                        possibleMoves[i] == KLAUSENBURG){
                        moveScore *= 2;
                    }
                }
                   
                printf(" %s.%d", idToName(possibleMoves[i]), moveScore);
                 // the location is not in the trail & there isnt any other hunter there
                if(moveScore > topScore){
                    printf("(!)");
                    topScore = moveScore;
                    registerBestPlay(idToAbbrev(possibleMoves[i]),"Marco!");
                }
            }
             


            //rest to find dracuuula
            if(giveMeTheRound(gameState)%6-1 == 0 && 
                giveMeTheRound(gameState) > 6 && currentHunter > 0 && posNum > 35){
                registerBestPlay(idToAbbrev(trail[0]),"Let me just sit for a second...");  
            } else if(giveMeTheRound(gameState)%6-2 == 0 && 
                giveMeTheRound(gameState) > 6 && currentHunter == 0 && posNum > 35){
                registerBestPlay(idToAbbrev(trail[0]),"Let me just sit for a second...");  
            }     
 
            printf(" - The last move is played.\n");
 
            free(possibleMoves);
             
        }
 
   // if first turn
    } else {
         
       
      printf("Starting position.\n");
      if (currentHunter == 0){
        registerBestPlay("CD","Come out come out wherever you are...");
      } else if (currentHunter == 1){
        registerBestPlay("CD","Come out come out wherever you are...");
      } else if (currentHunter == 2){
        registerBestPlay("FR","Come out come out wherever you are...");
      } else if (currentHunter == 3){
        registerBestPlay("MA","Come out come out wherever you are...");
      }
    }
}
 


LocationID mostLikelySea (HunterView gameState, LocationID from, int howFarFromLastSeen){
    
    LocationID destination;
    
    if (pathLength(gameState, from, MEDITERRANEAN_SEA) == howFarFromLastSeen) {
        destination = MEDITERRANEAN_SEA;
    } else if (pathLength(gameState, from, IONIAN_SEA) == howFarFromLastSeen) {
        destination = IONIAN_SEA;
    } else if (pathLength(gameState, from, NORTH_SEA) == howFarFromLastSeen) {
        destination = NORTH_SEA;
    } else if (pathLength(gameState, from, ADRIATIC_SEA) == howFarFromLastSeen) {
        destination = ADRIATIC_SEA;
    } else if (pathLength(gameState, from, ATLANTIC_OCEAN) == howFarFromLastSeen) {
        destination = ATLANTIC_OCEAN;
    } else if (pathLength(gameState, from, BAY_OF_BISCAY) == howFarFromLastSeen) {
        destination = BAY_OF_BISCAY;
    } else if (pathLength(gameState, from, BLACK_SEA) == howFarFromLastSeen) {
        destination = BLACK_SEA;
    } else if (pathLength(gameState, from, TYRRHENIAN_SEA) == howFarFromLastSeen) {
        destination = TYRRHENIAN_SEA;
    } else if (pathLength(gameState, from, ENGLISH_CHANNEL) == howFarFromLastSeen) {
        destination = ENGLISH_CHANNEL;
    } else if (pathLength(gameState, from, IRISH_SEA) == howFarFromLastSeen) {
        destination = IRISH_SEA;
    }
    
    return destination;
    
}


int pathLength(HunterView gameState, LocationID src, LocationID dest){
    
    
    int length = 1;
    
    int i;
    int *visited; // array [0..V-1] of boolean

    // initialise
    visited = malloc(NUM_MAP_LOCATIONS * sizeof(int));
    // initialised all values to 0
    
    for (i = 0; i < NUM_MAP_LOCATIONS; i++) visited[i] = 0;
     
    // create an array of locations that contain the position of it's pred.
    // the pred is the location that leads to the next position towards drac.
    LocationID *path = malloc(NUM_MAP_LOCATIONS * sizeof(LocationID));

    // initialise all values to -1
    for (i = 0; i < NUM_MAP_LOCATIONS; i++) path[i] = -1;


    // new queue for BFS
    Queue q = newQueue();
     
    // add source to the array
    QueueJoin(q, src);

    // add source to visited array
    visited[src] = 1;
     
    // an integer to suggest we found the destination
    int isFound = 0;

    // while the queue isn't empty && we haven't found the destination
    while ( emptyQueue(q) == 0 && isFound == 0) {
        // take the first value of the queue, remove it from the queue
        LocationID x = QueueLeave(q);
        LocationID y;
         
        // for every position on the map, we check if it's adjacent to a given position "x"
        for (y = 0; y < NUM_MAP_LOCATIONS; y++) {

            
            // if an edge doesn't exist b/t x & y, goto next iteration of position "y"
            if (!areAdjacent (gameState,x,y)) {
                continue;
            }

            int numPossibleMoves, i, notValid = 0;
            LocationID *possibleMoves = validAdjacent(gameState, &numPossibleMoves, x, 1, 1, 1);
             
            for( i=0; i < numPossibleMoves; i++){
                if (possibleMoves[i] == y) {
                    notValid = 1;
                    break;
                }
            }
             
            if (notValid != 1) {
                continue;
            }
             
            // link the current city to the adjacent city
            if (path[y] == -1){
                path[y] = x;
            }
             
            // if y is the location of drac, we've formed the shortest path.
            if (y == dest) {
                isFound = 1;
                // end the search here
                break;
            }
             
            // if we've already visited the city before, we don't add it to the queue
            // else we add the new city to the queue and mark it as visited
            if (visited[y] != 1) {
                QueueJoin(q, y);
                visited[y] = 1;
            }
            
            free(possibleMoves);
        }
    }
    free(visited);
     
     
    // since we found it...
    if (isFound) {
//        printf("path to potential dest is: \n");
        // display path in dest..src order
        LocationID v;
        // traverse from "drac's location" to "your current position"
        for (v = dest; path[v] != src; v = path[v]){
//            printf(" %s", idToAbbrev(v));
            length++;
        }
//        printf(" %s %s\n", idToAbbrev(v), idToAbbrev(src));
        
        free(q);
        free(path);
        // return the first position on the path towards drac.
        return length;
         
    } else {
        free(path);
        free(q);
        printf("failled path length\n");
        return -1;
    }
}
 
 
//finds shortest path to dracula's last sign using a queue
int findPath(HunterView gameState, LocationID src, LocationID dest)
{
    printf("beginning dest : %s\n",idToName(dest));
    int i;
    int *visited; // array [0..V-1] of boolean
    // initialise
    visited = malloc(NUM_MAP_LOCATIONS * sizeof(int));
    // initialised all values to 0
 
    for (i = 0; i < NUM_MAP_LOCATIONS; i++) visited[i] = 0;
     
    // create an array of locations that contain the position of it's pred.
    // the pred is the location that leads to the next position towards drac.
    LocationID *path = malloc(NUM_MAP_LOCATIONS * sizeof(LocationID));
    // initialise all values to -1
    for (i = 0; i < NUM_MAP_LOCATIONS; i++) path[i] = -1;
     
    // new queue for BFS
    Queue q = newQueue();
     
    // add source to the array
    QueueJoin(q, src);
     
    // add source to visited array
    visited[src] = 1;
     
    // an integer to suggest we found the destination
    int isFound = 0;
     
     
    // while the queue isn't empty && we haven't found the destination
    while ( emptyQueue(q) == 0 && isFound == 0) {
        // take the first value of the queue, remove it from the queue
        LocationID x = QueueLeave(q);
        LocationID y;
         
        // for every position on the map, we check if it's adjacent to a given position "x"
        for (y = 0; y < NUM_MAP_LOCATIONS; y++) {
 
            // if an edge doesn't exist b/t x & y, goto next iteration of position "y"
            if (!areAdjacent (gameState,x,y)) {
                continue;
            }
            int numPossibleMoves, i, notValid = 0;
            LocationID *possibleMoves = validAdjacent(gameState, &numPossibleMoves, x, 1, 1, 1);
      
            for( i=0; i < numPossibleMoves; i++){
                if (possibleMoves[i] == y) {
                    notValid = 1;
                    break;
                }
            }
 
            if (notValid != 1) {
                free(possibleMoves);
                continue;
            }
             
            // link the current city to the adjacent city
            if (path[y] == -1){
                path[y] = x;
            }
             
            // if y is the location of drac, we've formed the shortest path.
            if (y == dest) {
                isFound = 1;
                // end the search here
            free(possibleMoves);
                break;
            }
             
            // if we've already vqueueisited the city before, we don't add it to the queue
            // else we add the new city to the queue and mark it as visited
            if (visited[y] != 1) {
                QueueJoin(q, y);
                visited[y] = 1;
            }
             
            free(possibleMoves);
        }
    }
 
    free(visited);
    
    printf("dest : %s\n",idToName(dest));
     
    // since we found it...
    if (isFound) {
        printf("shortest path to drac is: \n");
        // display path in dest..src order
        LocationID v;
        // traverse from "drac's location" to "your current position"
        for (v = dest; path[v] != src; v = path[v])
            printf("%s ", idToName(v));
        printf(" %s %s\n", idToName(v), idToName(src));
         
//        free(q);
        free(path);
        // return the first position on the path towards drac.
        return v;
         
    } else {
        free(path);
//        free(q);
        printf("faiiled\n");
        return -1;
    }
}
