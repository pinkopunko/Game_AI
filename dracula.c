// dracula.c
// Implementation of your "Fury of Dracula" Dracula AI

#include <stdlib.h>
#include <stdio.h>
#include "Game.h"
#include "DracView.h"


/* FUNCTIONS OF DRACULA
 * 
 * 1. Path away from every hunter
 *    - need "distance chart" for every possible move (providing score)
 *       + remember trains!
 *
 * 2. favor edge of map away from Castle Dracula
 *    - The further away before Oh-shit the better
 *
 * 3. Oh-shit function:
 *    - Stucks self to get ported to Castle Dracula
 *    - if fewer hunters around castle drac (+hp & port far away)
 *    - possible from granada(-2), liverpool(-6), santander(-2) (anywhere with <=4 connected connections)
 *
 * 4. Favor cities over sea
 *    - don't wanna lose too much hp.
 *
 * 5. Clever use of sea?
 *    - dodge hunters - if surrounded?
 */

/*  - Start wherever is far away from all hunters
 *      - check against possible move for hunters?
 *  - Always move somewhere out of reach of the hunters
 *      - check move against possible move for hunters (distance #step 2?)
 *      - use sea only if "trapped"
 *  - Have option to get stuck/teleport & heal
 *      - Cagliari (alicante/barc/marsei/genoa/rome/naples - sea - sea)
 *      - Athens (sarajevo/salonica - valona - sea)
 *      - Plymouth (Le havre - englis channel - london)
 *  - try to let vampires spawn on land
 *      - every 13th round is more important that is on land (rng-gods?)
 */



static LocationID funkyTeleport(DracView gameState);
static int giveScore(DracView gameState, LocationID destination, LocationID *trail);

void decideDraculaMove(DracView gameState)
{
	//Where am I?
    LocationID trail[TRAIL_SIZE];
    giveMeTheTrail(gameState, 4, trail);
    int numPossibleMoves = 0;

    int moveScore, topScore = 0;

    // if not first turn
	if(trail[0]>-1){ 
   	    LocationID *possibleMoves = whereCanIgo(gameState, &numPossibleMoves, 1, 1);
        LocationID bestplay;
        int i, k;

        //prints information
        printf("\nRound: %d\t\tScore: %d\t\tHealth: %d\t\tLoc: %s\n",
            giveMeTheRound(gameState), giveMeTheScore(gameState), 
            howHealthyIs(gameState, 4), idToName(whereIs(gameState, 4)));
        printf("HunterLoc:\t\tG: %s\t\tS: %s\t\tH: %s\t\tM: %s\n", 
            idToName(whereIs(gameState, 0)), idToName(whereIs(gameState, 1)), 
            idToName(whereIs(gameState, 2)), idToName(whereIs(gameState, 3)));
        printf("HunterHP:\t\tG: %d\t\tS: %d\t\tH: %d\t\tM: %d\n", 
            howHealthyIs(gameState, 0), howHealthyIs(gameState, 1), 
            howHealthyIs(gameState, 2), howHealthyIs(gameState, 3));
        printf("Trail:\t%s  %s  %s  %s  %s  %s\n", idToName(trail[0]), idToName(trail[1]),
            idToName(trail[2]), idToName(trail[3]), idToName(trail[4]), idToName(trail[5]));


		printf("Possible Move:");
        for(i=0;i<numPossibleMoves;i++){
            if (possibleMoves[i]){
                printf("  %s",idToName(possibleMoves[i]));
            }
        }
        printf("\nHide: %d Double back: %d\n", drachide(gameState), dracback(gameState));

        printf("Registering move:");

        //Chose move
		for(i=0;i<numPossibleMoves;i++){

            int skip = 0;

            //if theres a hide/db and we try a move in the trail - continue
            if (possibleMoves[i] == trail[0] && drachide(gameState) && dracback(gameState)) skip++;
            if (possibleMoves[i] == trail[0] && dracback(gameState) && idToType(possibleMoves[i]) == 2) skip++;

            if (dracback(gameState)){
                for (k=1;k<5;k++){
                    if (trail[k] == possibleMoves[i] &&
                        trail[0] != possibleMoves[i] &&
                        dracback(gameState) != 5-k &&
                        drachide(gameState) != 5-k) skip++;
                }
            }
            if (skip) continue;

            //gets score of move
            moveScore = giveScore(gameState, possibleMoves[i], trail);

            //translates if needed
            for (k=0;k<6;k++) if (trail[k] == TELEPORT) trail[k] = CASTLE_DRACULA;

            if (possibleMoves[i] == trail[0] && !drachide(gameState) &&
                                     idToType(possibleMoves[i]) != 2) possibleMoves[i] = HIDE;
            if (possibleMoves[i] == trail[0] && !dracback(gameState)) possibleMoves[i] = DOUBLE_BACK_1;
            if (possibleMoves[i] == trail[1] && !dracback(gameState)) possibleMoves[i] = DOUBLE_BACK_2;
            if (possibleMoves[i] == trail[2] && !dracback(gameState)) possibleMoves[i] = DOUBLE_BACK_3;
            if (possibleMoves[i] == trail[3] && !dracback(gameState)) possibleMoves[i] = DOUBLE_BACK_4;
            if (possibleMoves[i] == trail[4] && !dracback(gameState)) possibleMoves[i] = DOUBLE_BACK_5;

            printf(" %s.%d", idToName(possibleMoves[i]), moveScore);


            //plays move is the score is better than the last one!
			if(moveScore > topScore){
                topScore = moveScore;
                bestplay = possibleMoves[i];
                printf("(!)");
				registerBestPlay(idToAbbrev(bestplay),"You never understood hide and seek, did you?");
			}
		}
   
        if (0){
            LocationID funk = funkyTeleport(gameState);
            registerBestPlay(idToAbbrev(funk),"Cant touch this.");
            printf(" %s", idToAbbrev(funk));
        }

        printf(" - The last move is played.\n");
		
		free(possibleMoves);

   // if first turn
	}else{
        //makes array for all locations
        LocationID locations[69];
        int i;
        for (i=0; i<69; i++) locations[i] = i;
        locations[59] = 70;

        //checks score & decides move
        printf("\nRegistering:");
        for (i=0; i<69; i++){
            moveScore = giveScore(gameState, locations[i], trail);

	        if(moveScore > topScore){
                topScore = moveScore;
                printf(" %s.%d", idToName(locations[i]), moveScore);
		        registerBestPlay(idToAbbrev(locations[i]),"Cant touch this.");
	        }
        }
    }
}


//sets up dracula to move to a location and teleport back to Castle Dracula
//    hardcoded to follow the top edge of the map all around until alicante
//    where it jumps into the sea and stucks self at cagliari
static LocationID funkyTeleport(DracView gameState)
{
    LocationID loc = whereIs(gameState, PLAYER_DRACULA), nextspot = -1;
    LocationID trail[TRAIL_SIZE];
    giveMeTheTrail(gameState, 4, trail);

    //stuck in cagliari between the seas #hardcode #win #perfect 26 turn cycle for never having vampire in sea

    //Check if hunters are close to Castle Dracula
    int c, k, hunterCD = 0, hunterAL = 0, hunterMovesNo;
    for (c=0; c<4; c++){
        LocationID *hunterMoves = whereCanTheyGo(gameState, &hunterMovesNo, c, 1, 1, 1);
        for (k=0; k<hunterMovesNo; k++){
            if(hunterMoves[k] == CASTLE_DRACULA ||
                hunterMoves[k] == KLAUSENBURG){
                hunterCD++;
            }
            if(hunterMoves[k] == ALICANTE){
                hunterAL++;
            }
        }
    }

    if (hunterCD > 1 || hunterAL > 1){
        nextspot = SARAGOSSA;
    } else {
        nextspot = HIDE;
    }

//    if(loc && nextspot)printf("");
    
    //gets stuck by Mediterraenean Sea
    if (/* STUCK AT MS */1){
        nextspot = DOUBLE_BACK_1;
    } else if (/* STUCK AT MS && drachide(gameState) == 6*/1){
        nextspot = DOUBLE_BACK_1;
    } else if (/* STUCK AT MS && dracback(gameState) == 6*/1){
        nextspot = MEDITERRANEAN_SEA;
    } else if (/* STUCK AT MS &&*/ loc == MEDITERRANEAN_SEA){
        nextspot = TYRRHENIAN_SEA;
    } else if (/* STUCK AT MS &&*/ loc == TYRRHENIAN_SEA){
        nextspot = CAGLIARI;
    }

    return nextspot;
}


//sets score of a given location to be compared against another
static int giveScore(DracView gameState, LocationID destination, LocationID *trail)
{
    //sets score of move (less if sea - especially if vampire is next up)
    int moveScore = 10000000;

    //more options = better
    LocationID numLocations;
    LocationID *loc = validAdjacent(gameState, &numLocations, destination, 1, 0, 1, 0);
    if (idToType(destination) != 2) moveScore += 200000*numLocations;
    else moveScore += 150000*numLocations;

    //reduce score if adjacent to hunter! (unless sea)
    int i, k, c, j, s=0;
    int dadjacent = 0, adjacent=0, adjacentsea=0, ontop=0, ontopsea=0;
    int hunterMovesNo, closetoCD = 0, closetoKL=0, closetoGZ=0;
    for (i=0; i<4; i++){
        LocationID *hunterMoves = whereCanTheyGo(gameState, &hunterMovesNo, i, 1, 1, 1);

        for (k=0; k<hunterMovesNo; k++){
            int numPotential;
            LocationID *hunterPotential = validAdjacent(gameState, &numPotential, hunterMoves[k], 1, 1, 1, 1);

            for (c=0; c<numPotential; c++){

                int numPotential2;
                LocationID *hunterPotential2 = validAdjacent(gameState, &numPotential2, hunterPotential[c], 1, 1, 1, 2);

                for (j=0; j<numPotential2; j++){
                    if (destination == hunterPotential2[j]){
                        moveScore -= 40000;
                        s++;
                    }
                    if (hunterPotential2[c] == CASTLE_DRACULA) closetoCD++;
                    if (hunterPotential2[c] == GALATZ) closetoGZ++;
                    if (hunterPotential2[c] == KLAUSENBURG) closetoKL++;
                }
                free(hunterPotential2);

                //adjacent to hunters possible move
                if (destination == hunterPotential[c]) dadjacent++;
            }
            free(hunterPotential);

            //adjacent to hunter
            if (destination == hunterMoves[k] && idToType(destination) != 2) adjacent++;
            else if (destination == hunterMoves[k]) adjacentsea++;
        }
        free(hunterMoves);

        //on top of hunter
        if (destination == whereIs(gameState, i) && idToType(destination) != 2) ontop++;
        else if (destination == whereIs(gameState, i)) ontopsea++;
    }
    closetoCD += closetoGZ + closetoKL;

    for (i=0; i<dadjacent; i++) moveScore *= 0.9;
    for (i=0; i<adjacent; i++) moveScore *= 0.1;
    for (i=0; i<adjacentsea; i++) moveScore *= 0.5;
    for (i=0; i<ontop; i++) moveScore *= 0.001;
    for (i=0; i<ontopsea; i++) moveScore *= 0.1;

    //new spot should have as few optional locs already in the trail as possible
    for (i=0; i<numLocations; i++){
        for (k=0; k<6; k++){
            if (loc[i]==trail[k]) moveScore *= 0.9;
        }
        if (idToType(loc[i]) == 2) moveScore *= 1.05;
        if (idToType(loc[i]) == 2 && (adjacent > 0 || dadjacent > 1)) moveScore *= 1.5;
        if (idToType(loc[i]) == 2 && (adjacent > 1 || dadjacent > 2)) moveScore *= 1.5;
    }

    //classic bad spot to get stuck :(
    if (destination == ATHENS) moveScore *= 0.05;
    if (destination == ADRIATIC_SEA && adjacent+adjacentsea < 1) moveScore *=0.5;
    if (destination == IRISH_SEA) moveScore *=0.9;
    if (destination == ENGLISH_CHANNEL) moveScore *=0.9;
    if ((destination == GALWAY || destination == DUBLIN) && idToType(trail[0]) == 2) moveScore *= 0.3;
    if (destination == (EDINBURGH || PLYMOUTH || LONDON || LIVERPOOL || SWANSEA) && idToType(trail[0]) == 2) moveScore *=0.7;
    if (trail[0] == IONIAN_SEA && destination == TYRRHENIAN_SEA) moveScore *= 3;
    if (adjacent+adjacentsea < 1 && destination == FLORENCE) moveScore *=0.5;

//    printf(" adj%d-s%d-cd%d", dadjacent, s, closetoCD);

    //sea = bad
    if (idToType(destination) == 2) moveScore *= 0.05;
    if ((giveMeTheRound(gameState)+1)%13 == 0 && idToType(destination) == 2) moveScore *= 0.2;
    if (idToType(destination) == 2 && destination == trail[0]) moveScore *= 0.1;
    if (idToType(destination) == 2 && howHealthyIs(gameState, 4) == 2) moveScore *= 0.001;
    
    //healing = good
    if (numLocations <= 3 && closetoCD < 10 && dracback(gameState) <= 1) moveScore *=2;
    else if (numLocations <= 3) moveScore *= 0.5;
    if (destination == CASTLE_DRACULA && closetoCD < 5) moveScore *=2;
    else if (destination == CASTLE_DRACULA) moveScore *= 0.5;
    if (destination == KLAUSENBURG && closetoCD < 10) moveScore *=2;
    if (destination == GALATZ && closetoCD < 10) moveScore *=2;

    for (i=0;i<5;i++) if (trail[i] == GALATZ && dracback(gameState)) i=10;
    if (destination == CASTLE_DRACULA && i != 10 && closetoGZ < closetoKL && closetoGZ == 0) moveScore *= 10;
    if (destination == CASTLE_DRACULA && howHealthyIs(gameState, 4) == 2) moveScore *= 10;

    //move in trail is less prefered
    for (i=0; i<6;i++) moveScore = destination == trail[i] ? moveScore*0.8 : moveScore;

    //do they know where I am?
    int seen = 6;
    for (i=0; i<6;i++){
        for (k=0; k<4;k++){
            LocationID hunttrail[TRAIL_SIZE];
            giveMeTheTrail(gameState, k, hunttrail);
            for (c=0; c<6;c++){
                if (seen==6 && (trail[i] == hunttrail[c] || trail[i] == CASTLE_DRACULA)) seen = i;
            }
        }
    }
    for (i=0; i<(6-seen);i++){
        for (k=0; k<6;k++) moveScore = destination == trail[k] ? moveScore*0.9 : moveScore;
    }

    //if trapped - sea = good
    if (dadjacent > 3 && idToType(destination) == 2 && howHealthyIs(gameState, 4) > 2) moveScore *= 2;
    if (dadjacent > 2 && idToType(destination) == 2 && howHealthyIs(gameState, 4) > 2) moveScore *= 2;
    if (adjacent+adjacentsea > 2 && idToType(destination) == 2 && howHealthyIs(gameState, 4) > 2) moveScore *= 2;
    if (adjacent+adjacentsea > 1 && idToType(destination) == 2 && howHealthyIs(gameState, 4) > 2) moveScore *= 4;
    if (adjacent+adjacentsea > 0 && idToType(destination) == 2 && howHealthyIs(gameState, 4) > 2) moveScore *= 4;

    free(loc);

    if (moveScore <= 0) moveScore = 1;

    return moveScore;
}
